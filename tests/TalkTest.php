<?php


namespace App\Tests;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Hautelook\AliceBundle\PhpUnit\RecreateDatabaseTrait;

class TalkTest extends ApiTestCase
{
    use RecreateDatabaseTrait;

    const API_TALKS = '/api/v1/talks';
    const API_SESSIONS = '/api/v1/sessions';
    const API_TRACKS = '/api/v1/tracks';

    public function testGetTalk(): void
    {
        $client = static::createClient();
        $response = $client->request('GET', self::API_TALKS);
        self::assertResponseIsSuccessful();
        self::assertEquals(14, $response->toArray()['hydra:totalItems']);
    }

    public function testCreateTalk(): void
    {
        $client = static::createClient();
        $body = [
            'title' => 'Domain-driven Design Architektur Kata',
            'length' => 60,
            'lang' => 'de',
        ];

        $response = $client->request('POST', self::API_TALKS, ['json' => $body])->toArray();

        $this->assertResponseStatusCodeSame(201);
        $this->assertEquals($body['title'], $response['title']);
        $this->assertEquals($body['length'], $response['length']);
        $this->assertEquals($body['lang'], $response['lang']);
    }

    public function testCreateInvalidLangTalk(): void
    {
        $client = static::createClient();
        $body = [
            'title' => 'Domain-driven Design Architektur Kata',
            'length' => 60,
            'lang' => 'wrongString',
        ];

        $response = $client->request('POST', self::API_TALKS, ['json' => $body]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertContains('lang: The type must be \u0022en\u0022 or \u0022de\u0022. \u0022wrongString\u0022 is not a valid type.', $response->getContent(false));
    }

    public function testCreateInvalidLengthTalk(): void
    {
        $client = static::createClient();
        $body = [
            'title' => 'Domain-driven Design Architektur Kata',
            'length' => 360,
            'lang' => 'en',
        ];

        $response = $client->request('POST', self::API_TALKS, ['json' => $body]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertContains('length: This value should be less than or equal to \u0022240\u0022.', $response->getContent(false));
    }

    public function testCreateTalkAndCheckTracks(): void
    {
        $client = static::createClient();
        $client->disableReboot();
        $client->request('POST', self::API_TALKS, ['json' => [
            'title' => 'Domain-driven Design Architektur Kata',
            'length' => 60,
            'lang' => 'de',
        ]]);
        $this->assertResponseStatusCodeSame(201);

        $tracks = $client->request('GET', self::API_TRACKS)->toArray();
        self::assertResponseIsSuccessful();
        self::assertEquals(2, $tracks['hydra:totalItems']);
        self::assertEquals(2, count($tracks['hydra:member'][0]['sessions']));
        self::assertEquals(2, count($tracks['hydra:member'][1]['sessions']));
    }

    public function testCreateTalkAndCheckSessions(): void
    {
        $client = static::createClient();
        $client->disableReboot();
        $client->request('POST', self::API_TALKS, ['json' => [
            'title' => 'Domain-driven Design Architektur Kata',
            'length' => 60,
            'lang' => 'de',
        ]]);
        $this->assertResponseStatusCodeSame(201);

        $sessions = $client->request('GET', self::API_SESSIONS)->toArray();
        self::assertResponseIsSuccessful();
        self::assertEquals(4, $sessions['hydra:totalItems']);
        foreach ($sessions['hydra:member'] as $session) {
            self::assertNotEmpty($session['type']);
            if('AM' == $session['type']) {
                self::assertLessThanOrEqual(180, $session['lengthTotal']);
            } else {
                self::assertLessThanOrEqual(240, $session['lengthTotal']);
            }
        }
    }
}