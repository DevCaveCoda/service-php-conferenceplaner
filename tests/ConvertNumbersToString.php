<?php


namespace App\Tests;


use PHPUnit\Framework\TestCase;

class ConvertNumbersToString extends TestCase
{
    use \App\Traits\ConvertNumbersToString;

    public function testConvertNumberToGerman(): void
    {
        $string = 'In diesem Text kommt die Zahl 23 vor';
        $result = $this->transformNumbersInStringToWords($string, 'de');
        self::assertEquals('In diesem Text kommt die Zahl dreiundzwanzig vor', $result);
    }

    public function testConvertNumberToEnglish(): void
    {
        $string = 'This text contains the two numbers 109 and 20';
        $result = $this->transformNumbersInStringToWords($string, 'en');
        self::assertEquals('This text contains the two numbers one hundred nine and twenty', $result);
    }
}