<?php


namespace App\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsLanguageType extends Constraint
{
    public $message = 'The type must be "en" or "de". "{{ string }}" is not a valid type.';
}