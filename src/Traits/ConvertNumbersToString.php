<?php


namespace App\Traits;


use NumberToWords\NumberToWords;

trait ConvertNumbersToString
{
    public function transformNumbersInStringToWords(string $string, string $language): string {
        $numberToWords = new NumberToWords();
        $numberTransformer = $numberToWords->getNumberTransformer($language);

        preg_match_all('/\d+/', $string, $matches);
        $numbersInTitle = $matches[0];

        $numberAsWord = [];
        foreach ($numbersInTitle as $number) {
            $numberAsWord[] = $numberTransformer->toWords($number);
        }
        foreach ($numbersInTitle as $key => $number) {
            $numbersInTitle[$key] = '/'.$number.'/';
        }

        return preg_replace($numbersInTitle, $numberAsWord, $string);
    }
}