<?php


namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Session;
use App\Entity\Talk;
use App\Entity\Track;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class TalkDataPersister implements ContextAwareDataPersisterInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * TalkDataPersister constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Is the data supported by the persister?
     * @param $data
     * @param array $context
     * @return bool
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Talk;
    }

    /**
     * Persists the data.
     *
     * @param Talk $data
     * @param array $context
     * @return Talk
     * @throws \Exception
     */
    public function persist($data, array $context = []): Talk
    {

        if (!$this->em->contains($data)) {
            $this->em->persist($data);
            $this->em->flush();
        }

        $this->buildTracks();

        $this->em->refresh($data);
        return $data;
    }

    /**
     * Removes the data.
     * @param Talk $data
     * @param array $context
     * @throws \Exception
     */
    public function remove($data, array $context = [])
    {
        $this->em->remove($data);
        $this->em->flush();

        $this->buildTracks();
    }

    /**
     * @throws \Exception
     */
    public function buildTracks(): void
    {
        $this->em->getRepository(Track::class)->deleteAll();
        $this->em->getRepository(Session::class)->deleteAll();
        $this->em->getRepository(Talk::class)->removeSessions();

        $talksWithoutSession = $this->em->getRepository(Talk::class)->findAllWithoutSession();
        do {
            $track = $this->prepareNewTrack($talksWithoutSession);
            $this->em->persist($track);
            $this->em->flush();
        } while (!empty($talksWithoutSession));
    }

    /**
     * @param Talk[] $talksWithoutSession
     * @return Track
     * @throws \Exception
     */
    public function prepareNewTrack(array &$talksWithoutSession): Track
    {
        $track = new Track();
        $amSession = $this->fillNewSession($talksWithoutSession, new Session("AM"), 180);
        $track->addSession($amSession);

        $pmSession = $this->fillNewSession($talksWithoutSession, new Session("PM"), 240);
        $track->addSession($pmSession);
        return $track;
    }

    /**
     * @param array $talksWithoutSession
     * @param Session $session
     * @param int $limit
     * @return Session
     * @throws \Exception
     */
    public function fillNewSession(array &$talksWithoutSession, Session $session, int $limit): Session
    {
        while (key_exists(0, $talksWithoutSession) && $session->getLengthTotal() < $limit) {
            if (($session->getLengthTotal() + $talksWithoutSession[0]->getLength()) > $limit) {
                break;
            }
            $session->addTalk($talksWithoutSession[0]);
            array_splice($talksWithoutSession, 0, 1);
        };
        return $this->setSessionTimes($session);
    }

    /**
     * @param Session $session
     * @return Session
     * @throws \Exception
     */
    public function setSessionTimes(Session $session): Session
    {
        $time = new DateTime();
        $dateInterval = new \DateInterval('PT' . $session->getLengthTotal() . 'M');

        if ('AM' === $session->getType()) {
            $endTime = clone $time->setTime(12, 00, 00);
            $startTime = $time->sub($dateInterval);
        } else {
            $startTime = clone $time->setTime(13, 00, 00);
            $endTime = $time->add($dateInterval);
        }

        $session->setStartTime($startTime);
        $session->setEndTime($endTime);
        return $session;
    }
}