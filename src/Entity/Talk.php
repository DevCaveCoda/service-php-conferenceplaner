<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Constraints as CustomAssert;
use App\Traits\ConvertNumbersToString;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(routePrefix="v1",
 *     normalizationContext={"groups"={
 *     "track:read",
 *     "session:read",
 *     "talk:read",
 *     },
 *     "swagger_definition_name": "read"},
 *     denormalizationContext={"groups"={
 *     "talk:write",
 *     },
 *     "swagger_definition_name": "write"},)
 * @ORM\Entity(repositoryClass="App\Repository\TalkRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Talk
{
    use ConvertNumbersToString;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"talk:read"})
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     * @Groups({"talk:read", "talk:write"})
     */
    private $title;

    /**
     * @Assert\NotNull()
     * @Assert\LessThanOrEqual("240")
     * @ORM\Column(type="integer")
     * @Groups({"talk:read", "talk:write"})
     */
    private $length;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Session", inversedBy="talks", cascade={"persist"})
     */
    private $session;

    /**
     * @CustomAssert\IsLanguageType
     * @ORM\Column(type="string", length=3)
     * @Groups({"talk:read", "talk:write"})
     */
    private $lang;

    /**
     * @ORM\PrePersist()
     */
    public function prePersist() {
        $transformedTitle = $this->transformNumbersInStringToWords($this->getTitle(), $this->getLang());
        $this->setTitle($transformedTitle);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getLength(): ?int
    {
        return $this->length;
    }

    public function setLength(int $length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getLang(): ?string
    {
        return $this->lang;
    }

    public function setLang(string $lang): self
    {
        $this->lang = $lang;

        return $this;
    }
}
