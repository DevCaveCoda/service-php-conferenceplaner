<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(routePrefix="v1",
 *     normalizationContext={"groups"={
 *     "session:read",
 *     "talk:read",
 *     },
 *     "swagger_definition_name": "read"},
 *     itemOperations={"get"},
 *     collectionOperations={"get"})
 * @ORM\Entity(repositoryClass="App\Repository\SessionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Session
{
    public function __construct($type)
    {
        $this->type = $type;
        $this->talks = new ArrayCollection();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"session:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"session:read"})
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"session:read"})
     */
    private $lengthTotal = 0;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Talk", mappedBy="session")
     * @Groups({"session:read"})
     */
    private $talks;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Track", inversedBy="sessions")
     */
    private $track;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Groups({"session:read"})
     */
    private $startTime;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Groups({"session:read"})
     */
    private $endTime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLengthTotal(): ?int
    {
        return $this->lengthTotal;
    }

    public function setLengthTotal(int $lengthTotal): self
    {
        $this->lengthTotal = $lengthTotal;

        return $this;
    }

    /**
     * @return Collection|Talk[]
     */
    public function getTalks(): Collection
    {
        return $this->talks;
    }

    public function addTalk(Talk $talk): self
    {
        if (!$this->talks->contains($talk)) {
            $this->talks[] = $talk;
            $talk->setSession($this);
            $this->setLengthTotal($this->getLengthTotal() + $talk->getLength());
        }

        return $this;
    }

    public function removeTalk(Talk $talk): self
    {
        if ($this->talks->contains($talk)) {
            $this->talks->removeElement($talk);
            // set the owning side to null (unless already changed)
            if ($talk->getSession() === $this) {
                $talk->setSession(null);
            }
        }

        return $this;
    }

    public function getTrack(): ?Track
    {
        return $this->track;
    }

    public function setTrack(?Track $track): self
    {
        $this->track = $track;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(?\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        return $this->endTime;
    }

    public function setEndTime(?\DateTimeInterface $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }
}
