<?php

namespace App\Repository;

use App\Entity\Session;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Session|null find($id, $lockMode = null, $lockVersion = null)
 * @method Session|null findOneBy(array $criteria, array $orderBy = null)
 * @method Session[]    findAll()
 * @method Session[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SessionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Session::class);
    }

    /**
     * @param $value
     * @return Session[]
     */
    public function findByTotalLengthLessThan($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.lengthTotal < :val')
            ->setParameter('val', $value)
            ->orderBy('s.lengthTotal', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param $value
     * @return Session[]
     */
    public function findByTotalLengthLessEqualsThan($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.lengthTotal <= :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param $value
     * @return Session[]
     */
    public function findByTotalLengthGreaterThan($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.lengthTotal > :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
            ;
    }

    public function deleteAll()
    {
        return $this->createQueryBuilder('s')
            ->delete('App:Session')
            ->getQuery()
            ->execute()
            ;
    }
}
