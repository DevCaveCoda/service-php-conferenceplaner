<?php

namespace App\Repository;

use App\Entity\Talk;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Talk|null find($id, $lockMode = null, $lockVersion = null)
 * @method Talk|null findOneBy(array $criteria, array $orderBy = null)
 * @method Talk[]    findAll()
 * @method Talk[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TalkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Talk::class);
    }

    /**
     * @param $value
     * @return Talk[]
     */
    public function findByLength($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.length = :val')
            ->setParameter('val', $value)
            ->orderBy('t.length', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Talk[]
     */
    public function findAllWithoutSession()
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.session is NULL')
            ->orderBy('t.length', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }
    /**
     * @return Talk[]
     */
    public function removeSessions()
    {
        return $this->createQueryBuilder('t')
            ->update('App:Talk', 't')
            ->set('t.session', '?1')
            ->setParameter(1, NULL)
            ->getQuery()
            ->execute()
            ;
    }
}
