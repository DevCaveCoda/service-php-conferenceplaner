Conference Planner Backend
----------- 
### Installation
To run the application following requirements must be fulfilled:
- PHP 7.1 or higher
- Composer

### Configuration
Before the application can be used following Steps are needed:

- composer install

Initialize the Database per doctrine:
- php bin/console doctrine:database:create --env=ENV
- php bin/console doctrine:schema:create --env=ENV

and if you want to setup test-data run the following:
- php bin/console hautelook:fixtures:load --env=ENV

or if a manual setup is done, configure the correct database and connection in the .env file
in the root folder and the doctrine.yaml in conf/packages.
Than generate the create-sql with 
- php bin/console doctrine:schema:create --env=ENV --dump-sql

and use this to Setup the database schema

### Local
The easiest way to run and test the application is in the dev-environment
than the symfony webserver-bundle is accessible and you can run the application with

- php bin/console server:run

### Usage
The api documentation is accessible under /api for example:

http://127.0.0.1:8000/api